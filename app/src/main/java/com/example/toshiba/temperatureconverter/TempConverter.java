package com.example.toshiba.temperatureconverter;

/**
 * Created by Toshiba on 7/13/2017.
 */

public class TempConverter {
    public static double celsius_Fahrenheit(double b){
        return 32+b*9/5;
    }

    public static double fahrenheit_celsius(double a){
        return (a-32)*5/9;
    }

}
