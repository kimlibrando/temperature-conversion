package com.example.toshiba.temperatureconverter;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TemperatureConverter extends AppCompatActivity {

    EditText temp;
    EditText answer;
    Button btnCtoF;
    Button btnFtoC;
    //boolean clicked=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature_converter);
        temp=(EditText)findViewById(R.id.tvEnterNumber);
        answer=(EditText)findViewById(R.id.tvAnswer);
        btnCtoF = (Button) findViewById(R.id.btnCtoF);
        btnFtoC = (Button) findViewById(R.id.btFtoC);
        btnCtoF.setOnClickListener(myhandler);
        btnFtoC.setOnClickListener(myhandler2);
        temp.setOnClickListener(myhandler3);
    }

    View.OnClickListener myhandler = new View.OnClickListener() {
        public void onClick(View v) {
            btnCtoF.setBackgroundColor(Color.RED);
            btnFtoC.setBackgroundColor(Color.GREEN);
            double value = new Double(temp.getText().toString());
            double ans=TempConverter.celsius_Fahrenheit(value);
            //answer.setText(new Double(ans).toString());
            answer.setText(String.format("%.2f", ans));
            }
    };

    View.OnClickListener myhandler2 = new View.OnClickListener() {
        public void onClick(View v) {
            btnFtoC.setBackgroundColor(Color.RED);
            btnCtoF.setBackgroundColor(Color.GREEN);
            double value = new Double(temp.getText().toString());
            double ans=TempConverter.fahrenheit_celsius(value);
            //answer.setText(new Double(Math.round(ans)).toString());
            answer.setText(String.format("%.2f", ans));
        }
    };

    View.OnClickListener myhandler3 = new View.OnClickListener() {
        public void onClick(View v) {
            btnFtoC.setBackgroundColor(Color.GREEN);
            btnCtoF.setBackgroundColor(Color.GREEN);
            answer.setText("Answer");
        }
    };

}